//
//  _0221025_OscarManzano_NYCSchoolUITests.swift
//  20221025-OscarManzano-NYCSchoolUITests
//
//  Created by Consultant on 10/26/22.
//

import XCTest

final class _0221025_OscarManzano_NYCSchoolUITests: XCTestCase {
    
    var app: XCUIApplication?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        try super.setUpWithError()
        self.app = XCUIApplication()

        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.app = nil
        try super.tearDownWithError()
    }

    func testNavigationwithScreenRecording(){
        // UI tests must launch the application that they test.
        self.app?.launch()
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Women's Academy of Excellence"]/*[[".cells.staticTexts[\"Women's Academy of Excellence\"]",".staticTexts[\"Women's Academy of Excellence\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Name: Women's Academy of Excellence").element/*[[".cells.containing(.staticText, identifier:\"Overview: The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!\").element",".cells.containing(.staticText, identifier:\"Address: 456 White Plains Road\").element",".cells.containing(.staticText, identifier:\"Website: schools.nyc.gov\/SchoolPortals\/08\/X282\").element",".cells.containing(.staticText, identifier:\"Email: sburns@schools.nyc.gov\").element",".cells.containing(.staticText, identifier:\"Phone: 718-542-0740\").element",".cells.containing(.staticText, identifier:\"Name: Women's Academy of Excellence\").element"],[[[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        
        let schoolsAroundNycButton = app.navigationBars["School Detail"].buttons["Schools Around NYC"]
        schoolsAroundNycButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Pan American International High School"]/*[[".cells.staticTexts[\"Pan American International High School\"]",".staticTexts[\"Pan American International High School\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        schoolsAroundNycButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["World Journalism Preparatory: A College Board School"]/*[[".cells.staticTexts[\"World Journalism Preparatory: A College Board School\"]",".staticTexts[\"World Journalism Preparatory: A College Board School\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Name: World Journalism Preparatory: A College Board School").element/*[[".cells.containing(.staticText, identifier:\"Overview: At World Journalism Preparatory, journalism is a tool used to share learning and practice the habits of critical thinking in regard to citizenship and democracy. Student publications include a school newspaper, magazine, news broadcast program, podcasts, websites, and blogs. Opportunities to be immersed in journalism exist through fieldwork, creative projects, performances, interviews, and readings set in the community. The school has writing labs for parents as well as students. Students visit television networks, newspapers, publishers, colleges, and other post-secondary schools.\").element",".cells.containing(.staticText, identifier:\"Address: 34-65 192nd Street\").element",".cells.containing(.staticText, identifier:\"Website: www.wjps.org\").element",".cells.containing(.staticText, identifier:\"Email: CSchneider2@schools.nyc.gov\").element",".cells.containing(.staticText, identifier:\"Phone: 718-461-2219\").element",".cells.containing(.staticText, identifier:\"Name: World Journalism Preparatory: A College Board School\").element"],[[[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        schoolsAroundNycButton.tap()
        
        let bronxHighSchoolForLawAndCommunityServiceStaticText = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Bronx High School for Law and Community Service"]/*[[".cells.staticTexts[\"Bronx High School for Law and Community Service\"]",".staticTexts[\"Bronx High School for Law and Community Service\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        bronxHighSchoolForLawAndCommunityServiceStaticText/*@START_MENU_TOKEN@*/.press(forDuration: 1.5);/*[[".tap()",".press(forDuration: 1.5);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"SAT TEST AVG.Score").element/*[[".cells.containing(.staticText, identifier:\"SAT Writing AVG.Score: 352\").element",".cells.containing(.staticText, identifier:\"SAT Math AVG.Score: 364\").element",".cells.containing(.staticText, identifier:\"SAT Critical Reading AVG.Score: 366\").element",".cells.containing(.staticText, identifier:\"SAT TEST AVG.Score\").element"],[[[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        schoolsAroundNycButton.tap()
        bronxHighSchoolForLawAndCommunityServiceStaticText.swipeUp()
        
        
                
        
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
