//
//  SchoolandSATViewModel.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import Foundation
import Combine


class SchoolandSATViewModel {
    
    private let networkService: NetworkService
    @Published var schoolList: [NYCSchool] = []
    @Published var SAT_Dict = [String: SATTest]()
    var subs = Set<AnyCancellable>()

    @Published var rowRequested = 0
    
    init(networkService: NetworkService = NetworkManager()) {
        self.networkService = networkService
    }
    
    func loadSchoolList() {
        self.networkService.getModel(url: NetworkParams.NYCSchoolList.url)
            .sink { completion in
                print(completion)
            } receiveValue: { (page: [NYCSchool]) in
                self.schoolList.append(contentsOf: page)
               
                print(page)
            }.store(in: &self.subs)
    }
    func loadSATList() {
        self.networkService.getModel(url: NetworkParams.SATList.url)
            .sink { completion in
                print(completion)
            } receiveValue: { (page: [SATTest]) in
                for p in page {
                    self.SAT_Dict[p.dbn] = p
                }
                
            }.store(in: &self.subs)
    }
    
    
}
