//
//  APIJsonStruct.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//


import Foundation

struct NYCSchool: Decodable{
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
    let ellPrograms: String
    let phoneNumber: String
    let faxNumber: String?
    let schoolEmail: String?
    let website: String
    let schoolSports: String?
    let primaryAddressLine1: String
    let city: String
    let zip: String
    let stateCode: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case ellPrograms = "ell_programs"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case schoolSports = "school_sports"
        case primaryAddressLine1 = "primary_address_line_1"
        case stateCode = "state_code"
        case website, dbn, city, zip
    }
}

struct SATTest: Decodable{
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case dbn
    }
}
