//
//  NetworkParams.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import Foundation

enum NetworkParams {
    case NYCSchoolList
    case SATList
    
    var url: URL? {
        switch self {
        case .NYCSchoolList:
            return URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
            
        case .SATList:
            return URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        }
    }
    
}
