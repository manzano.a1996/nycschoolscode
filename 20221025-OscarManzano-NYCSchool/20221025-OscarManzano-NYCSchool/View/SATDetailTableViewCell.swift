//
//  SATDetailTableViewCell.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import Foundation
import UIKit

class SATDetailTableViewCell: UITableViewCell {
    static let reusedId = "\(SATDetailTableViewCell.self)"
    
    lazy var TestLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "SAT TEST AVG.Score"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    

    lazy var satCriticalReadingAvgScoreLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "satCriticalReadingAvgScoreLabel"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    lazy var satMathAvgScoreLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "satMathAvgScoreLabel"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    lazy var satWritingAvgScoreLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "satWritingAvgScoreLabel"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.SetUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func SetUp(){
        self.addSubview(self.TestLabel)
        self.addSubview(self.satCriticalReadingAvgScoreLabel)
        self.addSubview(self.satMathAvgScoreLabel)
        self.addSubview(self.satWritingAvgScoreLabel)


        TestLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 8).isActive =  true
        TestLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        TestLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        satCriticalReadingAvgScoreLabel.topAnchor.constraint(equalTo: TestLabel.bottomAnchor, constant: 8).isActive = true
        satCriticalReadingAvgScoreLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        satCriticalReadingAvgScoreLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        satMathAvgScoreLabel.topAnchor.constraint(equalTo: satCriticalReadingAvgScoreLabel.bottomAnchor, constant: 8).isActive = true
        satMathAvgScoreLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        satMathAvgScoreLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        satWritingAvgScoreLabel.topAnchor.constraint(equalTo: satMathAvgScoreLabel.bottomAnchor, constant: 8).isActive = true
        satWritingAvgScoreLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        satWritingAvgScoreLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        satWritingAvgScoreLabel.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
    }

    
}
