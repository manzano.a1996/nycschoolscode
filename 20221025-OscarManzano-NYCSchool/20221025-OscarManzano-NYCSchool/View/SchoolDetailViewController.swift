//
//  SchoolDetailViewController.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Consultant on 10/26/22.
//

import Foundation
import UIKit

class SchoolDetailViewController: UIViewController {
    
    lazy var SchoolImage: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "schools")
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.borderWidth = 2.0
        imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        return imageView
    }()
    
    lazy var DetailTableView: UITableView = {
        let table = UITableView(frame: .zero)

        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = .black
        table.layer.cornerRadius = 10
        table.dataSource = self
        table.register(SchoolDetailTableViewCell.self, forCellReuseIdentifier: SchoolDetailTableViewCell.reusedId)
        table.register(SATDetailTableViewCell.self, forCellReuseIdentifier: SATDetailTableViewCell.reusedId)
        return table

    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        title = "School Detail"
        setUpUI()
        
    }
    
    private func setUpUI() {
        view.addSubview(self.SchoolImage)
        view.addSubview(self.DetailTableView)

        
        
        SchoolImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        SchoolImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        SchoolImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        SchoolImage.bottomAnchor.constraint(equalTo: DetailTableView.topAnchor, constant: -8).isActive = true
        
        
        DetailTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        DetailTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        DetailTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        
        
    }
    var SchoolN: String?
    var PhoneN: String?
    var SchoolE: String?
    var Website: String?
    var PrimaryA: String?
    var Overview: String?
    var satWriting: String?
    var satMath: String?
    var satCriticalReading: String?
    
    func info(schoolNameLabel: String, phoneNumberLabel: String, schoolEmailLabel: String, websiteLabel: String, primaryAddressLine1Label: String,  overviewParagraphLabel: String, SatWriting: String,SatMath: String, SatCriticalReading: String){
        SchoolN = schoolNameLabel
        PhoneN = phoneNumberLabel
        SchoolE = schoolEmailLabel
        Website = websiteLabel
        PrimaryA = primaryAddressLine1Label
        Overview = overviewParagraphLabel
        satWriting = SatWriting
        satMath = SatMath
        satCriticalReading = SatCriticalReading
        
         
    }
}

extension SchoolDetailViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row % 2) == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailTableViewCell.reusedId, for: indexPath) as? SchoolDetailTableViewCell else {
                return UITableViewCell()
            }
            cell.schoolNameLabel.text = "Name: \(SchoolN ?? "name")"
            cell.phoneNumberLabel.text = "Phone: \(PhoneN ?? "num")"
            cell.schoolEmailLabel.text = "Email: \(SchoolE ?? "email")"
            cell.websiteLabel.text = "Website: \(Website ?? "website")"
            cell.primaryAddressLine1Label.text = "Address: \(PrimaryA ?? "")"
            cell.overviewParagraphLabel.text = "Overview: \(Overview ?? "")"
            return cell
           
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SATDetailTableViewCell.reusedId, for: indexPath) as? SATDetailTableViewCell else {
                return UITableViewCell()
            }
            cell.satCriticalReadingAvgScoreLabel.text = "SAT Critical Reading AVG.Score: \(satCriticalReading ?? "satCriticalReading")"
            cell.satMathAvgScoreLabel.text = "SAT Math AVG.Score: \(satMath ?? "satMath")"
            cell.satWritingAvgScoreLabel.text = "SAT Writing AVG.Score: \(satWriting ?? "satWriting")"
            return cell
            
        }
        
    }
    
}


