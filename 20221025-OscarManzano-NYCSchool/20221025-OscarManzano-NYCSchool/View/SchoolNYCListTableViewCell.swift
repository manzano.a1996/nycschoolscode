//
//  SchoolNYCListTableViewCell.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import UIKit

class SchoolNYCListTableViewCell: UITableViewCell {

    static let reuseId = "\(SchoolNYCListTableViewCell.self)"
    
    
    lazy var SchoolTitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpUI() {
        self.contentView.addSubview(self.SchoolTitleLabel)
        

        self.SchoolTitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8).isActive = true
        self.SchoolTitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8).isActive = true
        self.SchoolTitleLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.SchoolTitleLabel.text = nil
    }

}
