//
//  SchoolDetailTableViewCell.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import Foundation
import UIKit

class SchoolDetailTableViewCell: UITableViewCell {
    static let reusedId = "\(SchoolDetailTableViewCell.self)"
    
    lazy var schoolNameLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "schoolName"
        labelN.numberOfLines = 0
        labelN.backgroundColor = .white
        labelN.layer.cornerRadius = 10

      
        return labelN
        
    }()
    lazy var overviewParagraphLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "overviewParagraph"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10

        return labelN
        
    }()
    lazy var phoneNumberLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "phoneNumber"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()

    lazy var schoolEmailLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "schoolEmail"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    lazy var websiteLabel: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "website"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()
    lazy var primaryAddressLine1Label: UILabel = {
        let labelN = UILabel(frame: .zero)
        labelN.translatesAutoresizingMaskIntoConstraints = false
        labelN.textAlignment = .center
        labelN.text = "primaryAddressLine1"
        labelN.numberOfLines = 0
        labelN.layer.cornerRadius = 10
        return labelN
        
    }()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.SetUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func SetUp(){
        self.addSubview(self.schoolNameLabel)
        self.addSubview(self.phoneNumberLabel)
        self.addSubview(self.schoolEmailLabel)
        self.addSubview(self.websiteLabel)
        self.addSubview(self.primaryAddressLine1Label)
        self.addSubview(self.overviewParagraphLabel)



        schoolNameLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 8).isActive =  true
        schoolNameLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        schoolNameLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        phoneNumberLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: 8).isActive = true
        phoneNumberLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        phoneNumberLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        schoolEmailLabel.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: 8).isActive = true
        schoolEmailLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        schoolEmailLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        websiteLabel.topAnchor.constraint(equalTo: schoolEmailLabel.bottomAnchor, constant: 8).isActive = true
        websiteLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        websiteLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        

        primaryAddressLine1Label.topAnchor.constraint(equalTo: websiteLabel.bottomAnchor, constant: 8).isActive = true
        primaryAddressLine1Label.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        primaryAddressLine1Label.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        
        overviewParagraphLabel.topAnchor.constraint(equalTo: primaryAddressLine1Label.bottomAnchor, constant: 8).isActive = true
        overviewParagraphLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive =  true
        overviewParagraphLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive =  true
        overviewParagraphLabel.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
    }
}

 
