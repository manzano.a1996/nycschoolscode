//
//  SchoolViewController.swift
//  20221025-OscarManzano-NYCSchool
//
//  Created by Oscar on 10/26/22.
//

import UIKit
import Combine

class SchoolViewController: UIViewController {
    
    
    lazy var SchoolTable: UITableView = {
        let table = UITableView(frame: .zero)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.dataSource = self
        table.delegate = self
        table.prefetchDataSource = self
        table.register(SchoolNYCListTableViewCell.self, forCellReuseIdentifier: SchoolNYCListTableViewCell.reuseId)
        
        return table
    }()
    
    let schoolandSATViewModels: SchoolandSATViewModel
    var subs = Set<AnyCancellable>()
    var count: Int?
    var countperpage = 25
    
    init(viewModel: SchoolandSATViewModel = SchoolandSATViewModel()) {
        self.schoolandSATViewModels = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        title = " Schools Around NYC"
        self.schoolandSATViewModels.$schoolList
            .receive(on: DispatchQueue.main)
            .sink { _ in
                self.SchoolTable.reloadData()
            }.store(in: &self.subs)
        self.schoolandSATViewModels.loadSchoolList()
        self.schoolandSATViewModels.loadSATList()
        
    }
    
    private func setUpUI() {
        self.view.backgroundColor = .white
        self.view.addSubview(self.SchoolTable)
        self.SchoolTable.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        self.SchoolTable.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        self.SchoolTable.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        self.SchoolTable.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
    }


}
extension SchoolViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailView = SchoolDetailViewController()
        var satmath = ""
        var satwriting = ""
        var satreading = ""
        let dbn = self.schoolandSATViewModels.schoolList[indexPath.row].dbn
        
        if let sat = self.schoolandSATViewModels.SAT_Dict[dbn] {
            satmath = "\(sat.satMathAvgScore)"
            satwriting = "\(sat.satWritingAvgScore)"
            satreading = "\(sat.satCriticalReadingAvgScore)"
        }

        if satmath == ""{
            satmath = "N/A"
            satwriting = "N/A"
            satreading = "N/A"
        }
        
        detailView.info(schoolNameLabel: self.schoolandSATViewModels.schoolList[indexPath.row].schoolName, phoneNumberLabel: self.schoolandSATViewModels.schoolList[indexPath.row].phoneNumber, schoolEmailLabel: self.schoolandSATViewModels.schoolList[indexPath.row].schoolEmail ?? "", websiteLabel: self.schoolandSATViewModels.schoolList[indexPath.row].website, primaryAddressLine1Label: self.schoolandSATViewModels.schoolList[indexPath.row].primaryAddressLine1, overviewParagraphLabel: self.schoolandSATViewModels.schoolList[indexPath.row].overviewParagraph,SatWriting: satwriting,SatMath: satmath,SatCriticalReading: satreading)

        self.navigationController?.pushViewController(detailView, animated: true)
    }
}

extension SchoolViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.count = self.schoolandSATViewModels.schoolList.count
        if countperpage < self.schoolandSATViewModels.schoolList.count{
            return countperpage
        }
        return self.schoolandSATViewModels.schoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolNYCListTableViewCell.reuseId, for: indexPath) as? SchoolNYCListTableViewCell else {
            return UITableViewCell()
        }
        
        cell.SchoolTitleLabel.text = self.schoolandSATViewModels.schoolList[indexPath.row].schoolName

        cell.accessoryType = .disclosureIndicator
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1

        let cc: UIColor = .black
        cell.layer.borderColor = cc.cgColor
        return cell
    }
    

    
}
extension SchoolViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let lastIndexPath = IndexPath(row: countperpage - 1, section: 0)
        guard indexPaths.contains(lastIndexPath) else { return }
        guard let count = self.count else {return}
        if countperpage <= count{
            self.countperpage = self.countperpage + 25
            self.SchoolTable.reloadData()        }
        
    }
    
}




